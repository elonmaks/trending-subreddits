const searchForm = document.querySelector('.section-navbar__search-box');
const searchInput = document.querySelector('.search-box__input');
const buttonContainer = document.querySelector('.main-content-box');
const buttonSort = buttonContainer.querySelectorAll('.sort-item');
const buttonActive = document.querySelector('.active');
let buttonActiveValue = buttonActive.querySelector('.sort-item-text').innerHTML;
const post = document.querySelector('.post');
const buttonToTop = document.querySelector('.main-content-sidebar__button');

//Get data fom server
const getData = url => {
    return fetch(url)
        .then(res => res.json())
        .then(data => data.data.children.map(data => data.data))
        .catch(err => console.log(err));
};

//Get subreddits
const getSubreddits = subreddits => {
    const url = `http://www.reddit.com/${subreddits.toLowerCase()}.json`;
    return getData(url);
};

//Get search content
const getSearchContent = searchTerm => {
    const url = `http://www.reddit.com/r/subreddit/search.json?q=${searchTerm}`;
    return getData(url);
};

//First call to show content of default active class
getSubreddits(buttonActiveValue)
    .then(results => {
        insertContent(results);
    });

//Active element event listener
for (let i = 0; i < buttonSort.length; i++) {
    buttonSort[i].addEventListener('click', () => {
        const current = document.getElementsByClassName('active');
        current[0].className = current[0].className.replace('active', '');
        buttonSort[i].className += ' active';
        buttonActiveValue = buttonSort[i].querySelector('.sort-item-text').innerHTML;
        getSubreddits(buttonActiveValue)
            .then(results => {
                insertContent(results);
            });
    });
};

//From Event Listener
searchForm.addEventListener('submit', e => {
    const searchTerm = searchInput.value;

    //Check input
    if (searchTerm === '') {
        alert('Please, fill search form');
    }

    searchInput.value = '';

    //Search reddit
    getSearchContent(searchTerm)
        .then(results => {
            insertContent(results);
        });
    e.preventDefault();
});

// Truncate Text    
const truncateText = (text, limit) => {
    const shortened = text.indexOf(' ', limit);
    if (shortened === -1) return text;
    return text.substring(0, shortened);
};

const insertContent = results => {
    let output = '<div class="main-content-posts">';
    results.forEach(post => {
        const contentText = post.selftext ? truncateText(post.selftext, 600) : textDefault();
        output += `
            <div class="post" onclick="window.location='${post.url}'">
                <div class="post-likes">
                        <i class="fas fa-arrow-alt-circle-up"></i>
                        <span class="post-likes-count">${post.score}</span>
                        <i class="fas fa-arrow-alt-circle-down"></i>
                </div>

                <div class="post-body">
                    <div class="post-body-author">
                        <span class="author-name">Posted by u/${post.subreddit}</span>
                        <span class="author-date">
                        Created: ${transformDate(post.created_utc)}
                        </span>
                    </div>

                    <div class="post-body-content">
                        <div class="content-wrapper">    
                            <div class="content-title">
                                <h2>Trending Subreddits for 
                                ${transformDate(post.created_utc)}: ${truncateText(post.title, 100)}
                                </h2>
                            </div>

                            <div class="content-description">
                                <p class="content-description-text">${contentText}</p>
                            </div>
                        </div>
                        <div class="content-description__bottom">
                            <div class="content-title__transparent">
                                <h2 class="transparent-title">
                                Trending Subreddits for ${transformDate(post.created_utc)}
                                </h2>
                            </div>

                            <div class="content-icons-wrap">
                                <div class="icons__bottom">
                                    <i class="fas fa-comment"></i>
                                    <span>${post.num_comments} comments</span>
                                </div>

                                <div class="icons__bottom">
                                    <i class="fas fa-award"></i>
                                    <span>Give Award</span>
                                </div>  

                                <div class="icons__bottom">
                                    <i class="fas fa-share-alt"></i>
                                    <span>Share</span>
                                </div>

                                <div class="icons__bottom">
                                    <i class="far fa-save"></i>
                                    <span>Save</span>
                                </div>

                                <div class="icons__bottom">
                                    <i class="fas fa-ellipsis-h"></i>
                                </div>
                            </div>
                        </div>
                    </div> 
                </div>
            </div>
        `;
    });
    output += '</div>'
    document.getElementById('results').innerHTML = output;
};

const textDefault = () => {
    return `What's this? We've started displaying a small
    selection of trending subreddits on the front page.
    Trending subreddits are determined based on a variety
    of activity indicators (which are also limited to
    safe for work communities for now). Subreddits can 
    choose to opt-out from consideration in their subreddit
    settings. We hope that you discover some interesting
    subreddits through this. Feel free to discuss other
    interesting or notable subreddits in the comment thread below.`
};

const transformDate = value => {
    const date = new Date();
    const year = date.getFullYear(value);
    let month = date.getMonth(value) + 1;
    const day = date.getDate(value);
    month < 9 ? month = `0${month}` : month;
    return `${year}-${month}-${day}`;
};

//Behavior of toTop button
const scrollFunction = () => {
    document.body.scrollTop > 300 || document.documentElement.scrollTop > 300 ?
        buttonToTop.style.display = 'block' :
        buttonToTop.style.display = 'none';
};

const toTop = () => {
    document.body.scrollTop = 0; // For Safari
    document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
};

window.onscroll = () => scrollFunction();
buttonToTop.addEventListener('click', toTop);